# Generated by Django 4.1.5 on 2023-01-15 08:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_alter_person_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='photo',
            field=models.ImageField(blank=True, default='person/person.svg', upload_to=''),
        ),
    ]
