from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.authentication import TokenAuthentication

from django.contrib.auth.models import User
from .serializers import UserSerializer, PersonSerializer
from .models import Person

# Create your views here.

# class PersonApiView(APIView):
#     permission_classes = [IsAuthenticated]
#     authentication_classes = [TokenAuthentication]
#     def get(self, request):
#         query = Person.objects.all()
#         serializer = PersonSerializer(query, many=True)
#         return Response(serializer.data)

class PersonViewSet(ModelViewSet):
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [TokenAuthentication]
    serializer_class = PersonSerializer
    def get_queryset(self):
        return Person.objects.all()

class ListUserViewSet(ModelViewSet):
    permission_classes = [IsAdminUser]
    # authentication_classes = [TokenAuthentication]
    serializer_class = UserSerializer
    def get_queryset(self):
        return User.objects.filter(is_staff=False)

class RegisterApiView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'error':False})
        return Response({'error': True, 'msg': serializer.errors})

class GetConnectedUser(APIView):
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [TokenAuthentication]
    def get(self, request):
        query = request.user
        serialiser = UserSerializer(query)
        return Response(serialiser.data)
