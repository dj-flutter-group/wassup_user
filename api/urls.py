from django.urls import path, include

from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

from .views import (
    RegisterApiView, ListUserViewSet, PersonViewSet,
    GetConnectedUser
) 

router = DefaultRouter()
router.register(r'persons', PersonViewSet, basename='persons')
router.register(r'list_users', ListUserViewSet, basename='list_users')

urlpatterns = [
    path('', include(router.urls)),
    # path('persons/', PersonApiView.as_view()),
    path('login/', obtain_auth_token),
    path('register/', RegisterApiView.as_view()),
    path('getUser/', GetConnectedUser.as_view()),
]